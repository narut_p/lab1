package oop.lab1;
/**
 * A simple model for a student with a name and id.
 * 
 * @author Narut Poovorakit
 */


public class Student extends Person {
	private long id;
	
	/**Initialize the new Student object
	 * @param name is the name of new Student
	 * @param id is the id of new Student
	 * 
	 */
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	/**
	 * Compare student by id.
	 * They are equal if the id matches.
	 * @param other is another id to compare to this one.
	 * @return true if the id is same, false otherwise.
	 */
	public boolean equals(Student other) {
		return this.id == other.id;
	}
}
